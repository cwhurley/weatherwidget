import React, { useState } from 'react';
import Container from './Containers/Container'
import { AppContext } from './util/context'

import './App.css';

export default function App() {
  const [location, setLocation] = useState('MELBOURNE');

  return (
    <AppContext.Provider value={{ location, setLocation }}>
      <Container />
    </AppContext.Provider>

  );
}
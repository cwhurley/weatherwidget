import React from 'react';
import styled from 'styled-components'
import TodayContent from './TodayContent'
import FooterContent from './TomorrowContent'
import { DataType } from '../types/types'

const Cards = styled.div `
position: absolute;
width: 664px;
height: 401px;
left: 65px;
top: 196px;`

type Props = {
  data: DataType;
};

export default function Card({ data }: Props) {
  return (
   <Cards data-testid='card'>
     <TodayContent todayData={data}/>
     <FooterContent tomorrowData={data}/>
   </Cards>
  );
}
import react from 'react'
import { render, getByText, getByTitle } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import Icon from '../Icon'

test('Icon Component Renders Correctly', () => {
  const todayData = { current: {condition : {icon: "//cdn.weatherapi.com/weather/64x64/day/116.png", text: 'Partly cloudy'}}}
  const { getByTitle } = render(<Icon altText={todayData.current.condition.text} url={todayData.current.condition.icon} left='70.93%' right='14.46%' top='21.45%' bottom='54.36%'/>

  )
  expect(getByTitle('Partly cloudy')).toBeTruthy() 
})


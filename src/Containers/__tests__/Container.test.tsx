import react from 'react'
import { render, waitFor, cleanup } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import Container from '../Container'
import axios from '../../util/Axios'

afterEach(cleanup)

test('Container renders correctly', async () => {
  const { getByText, getByTestId, debug } = render(
    <Container />
  )
  expect(getByTestId('MELBOURNE')).toHaveStyle(`color: #FFFFFF`)
  expect(getByTestId('SYDNEY')).toHaveStyle(`color: #000000`)
  expect(getByTestId('BRISBANE')).toHaveStyle(`color: #000000`)
  expect(getByText('Loading...')).toBeTruthy()
})

test('Mock successful api call', async () => {
  axios.get = jest.fn()
  const data = { data: { current: { condition: { text: 'Sunny', icon: '' }, temp_c: 29 }, forecast: { forecastday: { 0: { date: '2021-03-14', day: { maxtemp_c: 30, mintemp_c: 18 } }, 1: { date: '2021-03-15', day: { maxtemp_c: 25, mintemp_c: 13, condition: { text: '', icon: '' } } } } } } }
  axios.get.mockImplementationOnce(() => Promise.resolve(data))

  const { getByText, getByTestId, debug } = render(
    <Container />
  )
  await waitFor(() => getByText('Weather for the day'))
  expect(getByText('Sunny')).toBeTruthy()
  expect(axios.get).toHaveBeenCalledTimes(1)

})

test('Mock rejected api call', async () => {
  axios.get = jest.fn()
  const error = { message: 'Error loading data' }
  axios.get.mockImplementationOnce(() => Promise.reject(error))
  const { getByText, getByTestId, debug } = render(
    <Container />
  )
  await waitFor(() => getByText('Error loading data'))
  expect(axios.get).toHaveBeenCalledTimes(1)

})

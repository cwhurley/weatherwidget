import react from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import Locations from '../Locations'

test ('PillHolder Container Renders correctly', () => {
  const { queryAllByTestId } = render (<Locations/>)
  expect(queryAllByTestId('PillHolder')).toBeTruthy()
  expect(queryAllByTestId('MELBOURNE')).toBeTruthy()
  expect(queryAllByTestId('SYDNEY')).toBeTruthy()
  expect(queryAllByTestId('BRISBANE')).toBeTruthy()
})


import React from 'react';
import styled from 'styled-components'

type Props = {
  text: string;
  left: string;
  right?: string;
  top: string;
  bottom?: string;
  aria?: string
};

const Header = styled.h2<{ left: string, right?: string, top: string, bottom?: string }> `
position: absolute;
left: ${(props) => props.left};
right: ${(props) => props.right ? props.right : ''};
top: ${(props) => props.top};
bottom: ${(props) => props.bottom ? props.bottom : ''};

font-family: Ubuntu;
font-style: normal;
font-weight: 300;
font-size: 36px;
line-height: 41px;
letter-spacing: 0.015em;
text-transform: capitalize;
`

export default function HeaderText({ text, left, right, top, bottom, aria }: Props) {

  return (
   <Header aria-label={aria} left={left} right={right} top={top} bottom={bottom}>{text}</Header>
  )
}
import React, { useEffect, useState } from 'react';
import styled from 'styled-components'
import Locations from './Locations'
import Card from './Card'
import axios from '../util/Axios'
import { useAppContext } from '../util/context'
import { DataType } from '../types/types'
import HeaderText from '../components/HeaderText';

const Containers = styled.div`
position: absolute;
width: 794px;
height: 663px;
left: 18px;
top: 19px;
background: #FFFFFF;
border-radius: 8px;
`

export default function Container() {
  const { location } = useAppContext()
  const [data, setData] = useState({} as DataType)
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState('')

  useEffect(() => {
    setLoading(true)
    axios.get(`${location}&days=2&aqi=no&alerts=no`).then(res => {
      setData(res.data)
      setLoading(false)
    }).catch(error => {
      setError(error.message)
      setLoading(false)
    })
  }, [location])

  return (
    <div>
      <Containers>
        <Locations data-testid='PillHolder' />
        {!loading && error === ''
          ? <Card data={data} />
          : !loading && error !== '' ? <HeaderText aria={'Error message is: ' + error} text={error} left='10%' right='10%' top='40%' bottom=''/>
            : <HeaderText aria={'Content is loading'} text={'Loading...'} left='40%' top='40%'/>
        }
      </Containers>
    </div>
  )
}
import React from 'react';
import styled from 'styled-components'

type Props = {
  text: string;
  left: string;
  width: string;
  top: string;
  height: string;
};

const Temp = styled.h1<{ left: string, width: string, top: string, height: string }> `
position: absolute;
left: ${(props) => props.left};
width: ${(props) => props.width};
top: ${(props) => props.top};
height: ${(props) => props.height};

font-family: Ubuntu;
font-style: normal;
font-weight: 300;
font-size: 200px;
line-height: 230px;
display: flex;
align-items: center;
text-align: center;
letter-spacing: 0.015em;
text-transform: uppercase;
`
const Icon = styled.h3 `
position: absolute;
width: 103px;
height: 103px;
left: 420px;
top: 98px;

/* H3 */

font-family: Ubuntu;
font-style: normal;
font-weight: 300;
font-size: 96px;
line-height: 110px;
letter-spacing: 0.015em;
text-transform: uppercase;
`

export default function CurrentTemp({ text, left, width, top, height }: Props) {

  return (
    <>
   <Temp aria-label={'Current temperature is ' + text + '°'} left={left} width={width} top={top} height={height}>{text}</Temp>
   <Icon aria-hidden='true'>°</Icon>
   </>
  )
}
import { render, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import LocationButton from '../LocationButton'
import { AppContext } from '../../util/context'


const location = 'MELBOURNE'
const setLocation = jest.fn()

test('LocationButton Component Renders Correctly', () => {
  const { getByTestId } = render(<div><LocationButton active={location === 'MELBOURNE' ? true : false} text='MELBOURNE' left='0%' right='73.21%' top='0%' bottom='0%' />
    <LocationButton active={location === 'SYDNEY' ? true : false} text='SYDNEY' left='36.6%' right='36.6%' top='0%' bottom='0%' />
    <LocationButton active={location === 'BRISBANE' ? true : false} text='BRISBANE' left='73.21%' right='0%' top='0%' bottom='0%' /></div>)
  expect(getByTestId('MELBOURNE')).toHaveStyle(`background: #000000`)
  expect(getByTestId('BRISBANE')).toHaveStyle(`background: rgba(0, 0, 0, 0.04)`)
  expect(getByTestId('SYDNEY')).toHaveStyle(`background: rgba(0, 0, 0, 0.04)`)
})


test('Click LocationButton calls setActive function', () => {
  const { getByTestId, debug } = render(
    <AppContext.Provider value={{ location, setLocation }}>
      <LocationButton active={location === 'SYDNEY' ? true : false} text='SYDNEY' left='0%' right='73.21%' top='0%' bottom='0%' />
    </AppContext.Provider>
  )
  fireEvent.click(getByTestId('SYDNEYbutton'))
  expect(setLocation).toHaveBeenCalledTimes(1)
})

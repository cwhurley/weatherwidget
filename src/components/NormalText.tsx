import React from 'react';
import styled from 'styled-components'

type Props = {
  text: string;
  left: string;
  right: string;
  top: string;
  bottom: string;
  aria?: string;
};

const Text = styled.h5<{ left: string, right: string, top: string, bottom: string }> `
position: absolute;
left: ${(props) => props.left};
right: ${(props) => props.right};
top: ${(props) => props.top};
bottom: ${(props) => props.bottom};
width: 180px;
font-family: Ubuntu;
font-style: normal;
font-weight: 300;
font-size: 16px;
line-height: 18px;
letter-spacing: 0.015em;
text-transform: capitalize;
`

export default function NormalText({ text, left, right, top, bottom, aria }: Props) {

  return (
   <Text aria-label={aria} left={left} right={right} top={top} bottom={bottom}>{text}</Text>
  )
}
import react from 'react'
import { render, getByText } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import TomorrowContent from '../TomorrowContent'

test ('TomorrowContent Container Renders correctly', () => {
  const data = {current:{condition: {text:'Sunny', icon: ''}, temp_c: 29}, forecast: { forecastday: {0: {date: '2021-03-14', day: {maxtemp_c: 30, mintemp_c: 18}}, 1: {date: '2021-03-15', day: {maxtemp_c: 25, mintemp_c: 13, condition: {text:'', icon: ''}}}}}}
  const { getByText } = render (<TomorrowContent tomorrowData={data}/>)
  expect(getByText('15 March')).toBeTruthy()
  expect(getByText('25°')).toBeTruthy()
})


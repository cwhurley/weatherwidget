import React from 'react';
import styled from 'styled-components'
import LocationPill from '../components/LocationButton'
import { useAppContext } from '../util/context'

const Toggle = styled.div`position: absolute;
width: 664px;
height: 50px;
left: 65px;
top: 99px;
`

const Line = styled.div<{ left: string, right: string, top: string, bottom: string }> `
position: absolute;
left: ${(props) => props.left};
right: ${(props) => props.right};
top: ${(props) => props.top};
bottom: ${(props) => props.bottom};
border: 1px solid #000000;
`

export default function Locations() {
  const { location } = useAppContext()

  return (
    <Toggle>
      <LocationPill active={location === 'MELBOURNE' ? true : false} text='MELBOURNE' left='0%' right='73.21%' top='0%' bottom='0%'/>
      <Line left='26.64%' right='63.4%' top='50%' bottom='50%'/>
      <LocationPill active={location === 'SYDNEY' ? true : false}  text='SYDNEY' left='36.6%' right='36.6%' top='0%' bottom='0%'/>
      <Line left='63.24%' right='26.79%' top='50%' bottom='50%'/>
      <LocationPill active={location === 'BRISBANE' ? true : false}  text='BRISBANE' left='73.21%' right='0%' top='0%' bottom='0%'/>
    </Toggle>
  )
}
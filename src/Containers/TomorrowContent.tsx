import React from 'react';
import styled from 'styled-components'
import Bold from '../components/BoldText'
import NormalText from '../components/NormalText';
import TomorrowTemp from '../components/TomorrowTemp'
import Icon from '../components/Icon'
import { DataType } from '../types/types'

const Content = styled.div `
position: absolute;
left: 0%;
right: 0%;
top: 76.31%;
bottom: 0%;
color: #FFFFFF;
background: #21678E;
border-bottom-right-radius: 8px;
border-bottom-left-radius: 8px;`

type Props = {
  tomorrowData: DataType;
};

export default function FooterContent({ tomorrowData }: Props) {

  const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

  return (
   <Content>
     <Bold text='Forecast for tomorrow' left='4.52%' right='65.36%' top='30%' bottom='50%'/>
     <NormalText text={new Date(tomorrowData.forecast.forecastday[1].date).getDate() + ' ' + monthNames[(new Date(tomorrowData.forecast.forecastday[1].date).getMonth())]} left='4.52%' right='65.36%' top='55%' bottom='25%'/>
     <TomorrowTemp text={tomorrowData.forecast.forecastday[1].day.maxtemp_c + '°'} left='36.3%' right='45.33%' top='20%' bottom='20%'/>
     <NormalText aria={'Tomorrows weather condition will be ' + tomorrowData.forecast.forecastday[1].day.condition.text} text={tomorrowData.forecast.forecastday[1].day.condition.text} left='56.33%' right='23.5%' top='50%' bottom='30%'/>
     <Icon altText={tomorrowData.forecast.forecastday[1].day.condition.text} url={tomorrowData.forecast.forecastday[1].day.condition.icon} left='83.58%' right='5.27%' top='10%' bottom='20%'/>
   </Content>
  );
}
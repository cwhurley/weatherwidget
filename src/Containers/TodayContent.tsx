

import React from 'react';
import styled from 'styled-components'
import HeaderText from '../components/HeaderText'
import NormalText from '../components/NormalText';
import CurrentTemp from '../components/CurrentTemp'
import Icon from '../components/Icon'
import { DataType } from '../types/types'

const Content = styled.div`
position: absolute;
left: 0%;
right: 0%;
top: 0%;
bottom: 0%;
color: #000000;

background: linear-gradient(180deg, #FFD057 0%, rgba(240, 190, 143, 0) 100%);
border-radius: 8px;`

type Props = {
  todayData: DataType;
}
export default function TodayContent({ todayData }: Props) {
  const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"]
  return (
    <Content>
      <HeaderText text='Weather for the day' left='4.52%' right='16.42%' top='8.98%' bottom='83.04%' />
      <NormalText text={new Date(todayData.forecast.forecastday[0].date).getDate() + ' ' + monthNames[(new Date(todayData.forecast.forecastday[0].date).getMonth())]} left='4.52%' right='63.36%' top='21.45%' bottom='75.86%' />
      <HeaderText aria={'The high for today is ' + todayData.forecast.forecastday[0].day.maxtemp_c.toFixed(0) + '°'} text={todayData.forecast.forecastday[0].day.maxtemp_c.toFixed(0) + '°'} left='30px' right='' top='153px' bottom='' />
      <NormalText text='High' left='91px' right='' top='169px' bottom='' />
      <HeaderText aria={'The low for today is ' + todayData.forecast.forecastday[0].day.mintemp_c.toFixed(0) + '°'} text={todayData.forecast.forecastday[0].day.mintemp_c.toFixed(0) + '°'} left='28px' right='' top='213px' bottom='' />
      <NormalText text='Low' left='91px' right='' top='230px' bottom='' />
      <NormalText aria={'The current weather condition is ' + todayData.current.condition.text} text={todayData.current.condition.text} left='471px' right='' top='210px' bottom='' />
      <CurrentTemp text={todayData.current.temp_c.toFixed(0)} left='186px' width='232px' top='100px' height='147px' />
      <Icon altText={todayData.current.condition.text} url={todayData.current.condition.icon} left='70.93%' right='14.46%' top='21.45%' bottom='54.36%' />
    </Content>
  );
}

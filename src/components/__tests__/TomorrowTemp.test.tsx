import react from 'react'
import { render, getByText } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import TomorrowTemp from '../TomorrowTemp'

test('TomorrowTemp Component Renders Correctly', () => {
  const tomorrowData = { forecast: {forecastday: {1: {day: {maxtemp_c: 23.4}}}}}
  const { getByText } = render(<TomorrowTemp text={tomorrowData.forecast.forecastday[1].day.maxtemp_c + '°'} left='36.3%' right='45.33%' top='20%' bottom='20%'/>
  )
  expect(getByText('23.4°')).toBeTruthy() 
})

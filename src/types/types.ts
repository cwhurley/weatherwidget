export type DataType = {
  current: CurrentType;
  forecast: ForecastType;
  
}

export type CurrentType = {
  temp_c: number
  condition: ConditionType
}

export type ConditionType = {
  icon: string
  text: string
}

export type ForecastType = {
  forecastday: ForecastDayType
}

export type ForecastDayType = {
        0: DayType
        1: DayType
}

export type DayType = {
    date: Date
    day: {
      maxtemp_c: number
      mintemp_c: number
      condition: {
        icon: string
        text: string
      }
    }
}
import React from 'react';
import styled from 'styled-components'

type Props = {
  text: string;
  left: string;
  right: string;
  top: string;
  bottom: string;
  
};

const Text = styled.h3<{ left: string, right: string, top: string, bottom: string }> `
position: absolute;
left: ${(props) => props.left};
right: ${(props) => props.right};
top: ${(props) => props.top};
bottom: ${(props) => props.bottom};

font-family: Ubuntu;
font-style: normal;
font-weight: 300;
font-size: 48px;
line-height: 55px;
letter-spacing: 0.015em;
text-transform: capitalize;
`

export default function TomorrowTemp({ text, left, right, top, bottom }: Props) {

  return (
   <Text aria-label={'Tomorrow maximum temperature will be ' + text} left={left} right={right} top={top} bottom={bottom}>{text}</Text>
  )
}
import react from 'react'
import { render, getByText } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import HeaderText from '../HeaderText'

test('HeaderText Component Renders Correctly', () => {
  const todayData = { forecast: {forecastday : {0: {day: {maxtemp_c: 14.05}}}}}
  const { getByText } = render(<HeaderText text={todayData.forecast.forecastday[0].day.maxtemp_c.toFixed(0) + '°'} left='30px' right='' top='153px' bottom=''/>)
  expect(getByText('14°')).toBeTruthy() 
})

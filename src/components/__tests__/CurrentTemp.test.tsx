import react from 'react'
import { render, getByText } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import CurrentTemp from '../BoldText'

test('CurrentTemp Component Renders Correctly', () => {
  const todayData = { current: {temp_c : 15.03}}
  const { getByText } = render(<CurrentTemp text={todayData.current.temp_c.toFixed(0)} left='186px' width='232px' top='100px' height='147px'/>
  )
  expect(getByText('15')).toBeTruthy() 
})

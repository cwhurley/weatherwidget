import react from 'react'
import { render, getByText } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import Bold from '../BoldText'

test('Bold Component Renders Correctly', () => {
  const { getByText } = render(<Bold text='Forecast for tomorrow' left='4.52%' right='65.36%' top='30%' bottom='50%'/>)
  expect(getByText('Forecast for tomorrow')).toBeTruthy() 
})

import React from 'react';
import styled from 'styled-components'

type Props = {
  url: string;
  left: string;
  right: string;
  top: string;
  bottom: string;
  altText: string;
};

const IconSpan = styled.span<{url:string, left: string, right: string, top: string, bottom: string }> `
position: absolute;
left: ${(props) => props.left};
right: ${(props) => props.right};
top: ${(props) => props.top};
bottom: ${(props) => props.bottom};
background: url(${(props) => props.url});
background-repeat: no-repeat;
    background-size: 100% 100%;
`

export default function CurrentTemp({ url, left, right, top, bottom, altText }: Props) {

  return (
   <IconSpan title={altText} url={url} left={left} right={right} top={top} bottom={bottom}/>
  )
}
import React from 'react';
import styled from 'styled-components'
import { useAppContext } from '../util/context'

type Props = {
  text: string;
  left: string;
  right: string;
  top: string;
  bottom: string;
  active: boolean;
};

const Toggle = styled.div<{ left: string, right: string, top: string, bottom: string }> `
position: absolute;
left: ${(props) => props.left};
right: ${(props) => props.right};
top: ${(props) => props.top};
bottom: ${(props) => props.bottom};
text-align: center;

`

const Rectangle = styled.button<{active: boolean}> `
width: 100%;
position: absolute;
left: 0%;
right: 0%;
top: 0%;
bottom: 0%;
background: ${props => props.active === true ? "#000000" : "rgba(0, 0, 0, 0.04)"};
border: 2px solid #000000;
box-sizing: border-box;
border-radius: 25px;
color: ${props => props.active === true ? "#FFFFFF" : "#000000"}
`

const Label = styled.label `
position: absolute;
left: 16.28%;
right: 16.86%;
top: 29%;
bottom: 36%;

/* Toggle lable */

font-family: Ubuntu;
font-style: normal;
font-weight: 500;
font-size: 18px;
line-height: 21px;
align-items: center;
text-align: center;
letter-spacing: 0.015em;
text-transform: uppercase;
`

export default function LocationPill({ text, left, right, top, bottom, active }: Props) {
  const { setLocation } = useAppContext()

  return (
   <Toggle data-testid={text + 'button'} onClick={() => setLocation(text)} left={left} right={right} top={top} bottom={bottom}><Rectangle aria-label={'Forecast for ' + text} data-testid={text} active={active}><Label>{text}</Label></Rectangle></Toggle>
  )
}
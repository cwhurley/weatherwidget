import { useContext, createContext } from 'react'

/**
 * Creates the context with the values and setting functions to be used throughout the application.
 */
export const AppContext = createContext({
  location: 'MELBOURNE',
  setLocation: (name: string) => {}
})

export function useAppContext () {
  return useContext(AppContext)
}

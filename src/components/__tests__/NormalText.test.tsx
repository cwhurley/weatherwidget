import react from 'react'
import { render, getByText } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import NormalText from '../NormalText'

test('NormalText Component Renders Correctly', () => {
  const todayData = { current: {condition : {text: 'Partly cloudy'}}}
  const { getByText } = render(<NormalText text={todayData.current.condition.text} left='471px' right='' top='210px' bottom=''/>)
  expect(getByText('Partly cloudy')).toBeTruthy() 
})

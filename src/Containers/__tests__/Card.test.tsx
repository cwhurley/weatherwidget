import react from 'react'
import { render, getByText } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import Card from '../Card'

test ('Card Container Renders correctly', () => {
  const data = {current:{condition: {text:'Sunny', icon: ''}, temp_c: 29}, forecast: { forecastday: {0: {date: '2021-03-14', day: {maxtemp_c: 30, mintemp_c: 18}}, 1: {date: '2021-03-15', day: {maxtemp_c: 25, mintemp_c: 13, condition: {text:'', icon: ''}}}}}}
  const { getByText } = render (<Card data={data}/>)
  expect(getByText('14 March')).toBeTruthy()
  expect(getByText(29)).toBeTruthy()
  expect(getByText('15 March')).toBeTruthy()
  expect(getByText('Sunny')).toBeTruthy()
})


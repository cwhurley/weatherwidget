# WeatherWidget

## Scripts

`npm run test` Will run all of the test files.

`npm run start` To start up the application in dev mode.

## Tests
Both containers and components have simple testing applied to them. They're tested to make sure they render correctly and functions have been called, either on load or after an event.

## Accessibility
I added appropriate aria labels to certain components so they make more sense when being read out by a screen reader. Using the silktide tool, I tested different colour blindness situations and the contrast of colours of the design maintained the legibility of the application.
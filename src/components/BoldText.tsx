import React from 'react';
import styled from 'styled-components'

type Props = {
  text: string;
  left: string;
  right: string;
  top: string;
  bottom: string;
};

const Text = styled.h4<{ left: string, right: string, top: string, bottom: string }> `
position: absolute;
left: ${(props) => props.left};
right: ${(props) => props.right};
top: ${(props) => props.top};
bottom: ${(props) => props.bottom};

font-family: Ubuntu;
font-style: normal;
font-weight: 500;
font-size: 16px;
line-height: 18px;
letter-spacing: 0.015em;
text-transform: capitalize;
`

export default function Bold({ text, left, right, top, bottom }: Props) {

  return (
   <Text left={left} right={right} top={top} bottom={bottom}>{text}</Text>
  )
}